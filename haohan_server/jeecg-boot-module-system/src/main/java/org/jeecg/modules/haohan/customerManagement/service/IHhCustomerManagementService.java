package org.jeecg.modules.haohan.customerManagement.service;

import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.haohan.customerManagement.entity.HhCustomerManagement;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 客户管理
 * @Author: jeecg-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
public interface IHhCustomerManagementService extends IService<HhCustomerManagement> {

	Result<?> customerAnalysis();
}
