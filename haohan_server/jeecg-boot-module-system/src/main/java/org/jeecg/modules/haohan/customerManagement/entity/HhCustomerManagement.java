package org.jeecg.modules.haohan.customerManagement.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 客户管理
 * @Author: jeecg-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
@Data
@TableName("hh_customer_management")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="hh_customer_management对象", description="客户管理")
public class HhCustomerManagement implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
	/**客户名称*/
	@Excel(name = "客户名称", width = 15)
    @ApiModelProperty(value = "客户名称")
    private String name;
	/**客户地址*/
	@Excel(name = "客户地址", width = 15)
    @ApiModelProperty(value = "客户地址")
    private String adder;
	/**客户状态*/
	@Excel(name = "客户状态", width = 15, dicCode = "customer_status")
	@Dict(dicCode = "customer_status")
    @ApiModelProperty(value = "客户状态")
    private Integer status;
	/**客户来源*/
	@Excel(name = "客户来源", width = 15, dicCode = "customer_source")
	@Dict(dicCode = "customer_source")
    @ApiModelProperty(value = "客户来源")
    private Integer source;
	/**所属行业*/
	@Excel(name = "所属行业", width = 15, dicCode = "industry")
	@Dict(dicCode = "industry")
    @ApiModelProperty(value = "所属行业")
    private Integer industry;
	/**服务项目*/
	@Excel(name = "服务项目", width = 15, dicCode = "service_project")
	@Dict(dicCode = "service_project")
    @ApiModelProperty(value = "服务项目")
    private Integer server;
	/**跟进人*/
	@Excel(name = "跟进人", width = 15)
    @ApiModelProperty(value = "跟进人")
    private String dockingPeople;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private String remark;
	/**图片*/
	@Excel(name = "图片", width = 15)
    @ApiModelProperty(value = "图片")
    private String photo;
}
