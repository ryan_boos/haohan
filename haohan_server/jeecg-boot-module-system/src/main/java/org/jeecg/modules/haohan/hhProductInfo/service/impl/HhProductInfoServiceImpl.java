package org.jeecg.modules.haohan.hhProductInfo.service.impl;

import org.jeecg.modules.haohan.hhProductInfo.entity.HhProductInfo;
import org.jeecg.modules.haohan.hhProductInfo.mapper.HhProductInfoMapper;
import org.jeecg.modules.haohan.hhProductInfo.service.IHhProductInfoService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 产品信息管理
 * @Author: jeecg-boot
 * @Date:   2022-02-10
 * @Version: V1.0
 */
@Service
public class HhProductInfoServiceImpl extends ServiceImpl<HhProductInfoMapper, HhProductInfo> implements IHhProductInfoService {

}
