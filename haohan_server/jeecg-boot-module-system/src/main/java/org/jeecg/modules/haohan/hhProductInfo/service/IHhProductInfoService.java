package org.jeecg.modules.haohan.hhProductInfo.service;

import org.jeecg.modules.haohan.hhProductInfo.entity.HhProductInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 产品信息管理
 * @Author: jeecg-boot
 * @Date:   2022-02-10
 * @Version: V1.0
 */
public interface IHhProductInfoService extends IService<HhProductInfo> {

}
