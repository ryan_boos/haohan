package org.jeecg.modules.haohan.hhProductInfo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.haohan.hhProductInfo.entity.HhProductInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 产品信息管理
 * @Author: jeecg-boot
 * @Date:   2022-02-10
 * @Version: V1.0
 */
public interface HhProductInfoMapper extends BaseMapper<HhProductInfo> {

}
