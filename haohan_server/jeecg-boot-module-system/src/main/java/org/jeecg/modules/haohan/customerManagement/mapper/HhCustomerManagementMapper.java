package org.jeecg.modules.haohan.customerManagement.mapper;

import org.jeecg.modules.haohan.customerManagement.entity.HhCustomerManagement;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * @Description: 客户管理
 * @Author: jeecg-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
public interface HhCustomerManagementMapper extends BaseMapper<HhCustomerManagement> {

	List<Integer> customerAnalysis();
}
