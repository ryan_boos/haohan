package org.jeecg.modules.haohan.customerManagement.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.haohan.customerManagement.entity.HhCustomerManagement;
import org.jeecg.modules.haohan.customerManagement.mapper.HhCustomerManagementMapper;
import org.jeecg.modules.haohan.customerManagement.service.IHhCustomerManagementService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description: 客户管理
 * @Author: jeecg-boot
 * @Date: 2022-01-19
 * @Version: V1.0
 */
@Service
public class HhCustomerManagementServiceImpl extends ServiceImpl<HhCustomerManagementMapper, HhCustomerManagement> implements IHhCustomerManagementService {

	@Override
	public Result<?> customerAnalysis() {
		List<Integer> lists = this.baseMapper.customerAnalysis();
		if (lists.isEmpty()) {
//			throw  new IllegalAccessException("没有客户数据!");
			return Result.error("没有客户数据!");
		}
		System.out.println(">>>" + lists.toString());
//		潜在客户	1
//		已成交客户	3
//		失效客户	2
//		VIP客户	4
//		普通客户	5
//		合作客户	6
		int qz = 0;
		int cj = 0;
		int sx = 0;
		int vip = 0;
		int pt = 0;
		int hz = 0;

		List<Map<String, Integer>> maps = new ArrayList<>();
		for (Integer list : lists) {
			if (list == 1) {
				qz++;
			}
			if (list == 2) {
				cj++;
			}
			if (list == 3) {
				sx++;
			}
			if (list == 4) {
				vip++;
			}
			if (list == 5) {
				pt++;
			}
			if (list == 6) {
				hz++;
			}
		}


		Map<String, Integer> map1 = new HashMap<>();
		map1.put("潜在客户", qz);
		maps.add(map1);

		Map<String, Integer> map6 = new HashMap<>();
		map6.put("失效客户", sx);
		maps.add(map6);

		Map<String, Integer> map2 = new HashMap<>();
		map2.put("已成交客户", cj);
		maps.add(map2);

		Map<String, Integer> map3 = new HashMap<>();
		map3.put("Vip客户", vip);
		maps.add(map3);

		Map<String, Integer> map4 = new HashMap<>();
		map4.put("普通客户", pt);
		maps.add(map4);

		Map<String, Integer> map5 = new HashMap<>();
		map5.put("合作客户", hz);
		maps.add(map5);

		return Result.OK(maps);
	}
}
